# GitLab CI template for Cypress

This project implements a GitLab CI/CD template to run your automated (web) tests with [Cypress](https://www.cypress.io/).

## Usage

This template can be used both as a [CI/CD component](https://docs.gitlab.com/ee/ci/components/#use-a-component-in-a-cicd-configuration) 
or using the legacy [`include:project`](https://docs.gitlab.com/ee/ci/yaml/index.html#includeproject) syntax.

### Use as a CI/CD component

Add the following to your `gitlab-ci.yml`:

```yaml
include:
  # 1: include the component
  - component: gitlab.com/to-be-continuous/cypress/gitlab-ci-cypress@3.6.1
    # 2: set/override component inputs
    inputs:
      review-enabled: true # ⚠ this is only an example
```

### Use as a CI/CD template (legacy)

Add the following to your `gitlab-ci.yml`:

```yaml
include:
  # 1: include the template
  - project: 'to-be-continuous/cypress'
    ref: '3.6.1'
    file: '/templates/gitlab-ci-cypress.yml'

variables:
  # 2: set/override template variables
  REVIEW_ENABLED: "true" # ⚠ this is only an example
```

## `cypress` job

This job starts [Cypress](https://www.cypress.io/) (functional) tests.

It uses the following variable:

| Input / Variable      | Description                              | Default value     |
| --------------------- | ---------------------------------------- | ----------------- |
| `image` / `CYPRESS_IMAGE` | The Docker image used to run Cypress (use [included images](https://github.com/cypress-io/cypress-docker-images/tree/master/included) only). | `registry.hub.docker.com/cypress/included:12.0.2` |
| `project-dir` / `CYPRESS_PROJECT_DIR` | The Cypress project directory (containing `cypress.config.js` or `cypress.config.ts`) | `.` |
| `extra-args` / `CYPRESS_EXTRA_ARGS` | Cypress extra [run options](https://docs.cypress.io/guides/guides/command-line.html#cypress-run) (to select a different browser, configuration or spec files for e.g.) | _none_ |
| `review-enabled` / `REVIEW_ENABLED` | Set to `true` to enable Cypress tests on review environments (dynamic environments instantiated on development branches) | _none_ (disabled) |

In addition to a textual report in the console, this job produces the following reports, kept for one day:

| Report         | Format                                                                       | Usage             |
| -------------- | ---------------------------------------------------------------------------- | ----------------- |
| `$CYPRESS_PROJECT_DIR/reports/cypress-*.xunit.xml` | [xUnit](https://github.com/jest-community/jest-junit#readme) test report(s) | [GitLab integration](https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportsjunit) |

### Cypress `baseUrl` auto evaluation

By default, the Cypress template tries to auto-evaluate the [baseUrl setting](https://docs.cypress.io/guides/references/best-practices.html#Setting-a-global-baseUrl)
(i.e. the variable pointing at server under test) by looking either for a `$environment_url` variable or for an 
`environment_url.txt` file.

Therefore if an upstream job in the pipeline deployed your code to a server and propagated the deployed server url,
either through a [dotenv](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#artifactsreportsdotenv) variable `$environment_url` 
or through a basic `environment_url.txt` file, then the Cypress test will automatically be run on this server.

:warning: all our deployment templates implement this design. Therefore even purely dynamic environments (such as review
environments) will automatically be propagated to your Cypress tests.

If you're not using a smart deployment job, you may still explicitly declare the `CYPRESS_BASE_URL` variable (but that 
will be unfortunately hardcoded to a single server).

### Hook scripts

The Cypress template supports _optional_ **hook scripts** from your project, located in the `$CYPRESS_PROJECT_DIR` directory to perform additional project-specific logic:

* `pre-cypress.sh` is executed **before** running Cypress,
* `post-cypress.sh` is executed **after** running Cypress (whichever the tests status).
