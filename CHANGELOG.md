## [3.6.1](https://gitlab.com/to-be-continuous/cypress/compare/3.6.0...3.6.1) (2024-05-05)


### Bug Fixes

* **workflow:** disable MR pipeline from prod & integ branches ([071bd70](https://gitlab.com/to-be-continuous/cypress/commit/071bd708eb8dd1ea39245fca0d869096dc2f4e67))

# [3.6.0](https://gitlab.com/to-be-continuous/cypress/compare/3.5.0...3.6.0) (2024-04-29)


### Features

* add hook scripts support ([0633afc](https://gitlab.com/to-be-continuous/cypress/commit/0633afc6e24f1a916f677fad1997ef848aa79780))

# [3.5.0](https://gitlab.com/to-be-continuous/cypress/compare/3.4.0...3.5.0) (2024-1-27)


### Features

* migrate to CI/CD component ([21a1fcd](https://gitlab.com/to-be-continuous/cypress/commit/21a1fcd2203bde0f2d0133c83dbd7cefdfec6ec7))

# [3.4.0](https://gitlab.com/to-be-continuous/cypress/compare/3.3.1...3.4.0) (2023-12-8)


### Features

* use centralized tracking image (gitlab.com) ([f9cc9a1](https://gitlab.com/to-be-continuous/cypress/commit/f9cc9a14dfac5d2916c828b28728c186f14c55bd))

## [3.3.1](https://gitlab.com/to-be-continuous/cypress/compare/3.3.0...3.3.1) (2023-10-16)


### Bug Fixes

* declare all TBC stages ([0e8754f](https://gitlab.com/to-be-continuous/cypress/commit/0e8754fe06518b20fadb60435f1720abd38d9b90))

# [3.3.0](https://gitlab.com/to-be-continuous/cypress/compare/3.2.1...3.3.0) (2023-05-27)


### Features

* **workflow:** extend (skip ci) feature ([cef80ef](https://gitlab.com/to-be-continuous/cypress/commit/cef80ef3829ff262a467f9fcba093156cd283bce))

## [3.2.1](https://gitlab.com/to-be-continuous/cypress/compare/3.2.0...3.2.1) (2023-01-27)


### Bug Fixes

* "Add registry name in all Docker images" ([6be1ba8](https://gitlab.com/to-be-continuous/cypress/commit/6be1ba8dbec5dd57904c4a8a9a0706ce979ee076))

# [3.2.0](https://gitlab.com/to-be-continuous/cypress/compare/3.1.1...3.2.0) (2022-12-11)


### Features

* upgrade cypress and config ([d4f3011](https://gitlab.com/to-be-continuous/cypress/commit/d4f3011733c46ee91808a6635b688aac917bacd8))

## [3.1.1](https://gitlab.com/to-be-continuous/cypress/compare/3.1.0...3.1.1) (2022-11-26)


### Bug Fixes

* support custom CA certificates with npm ([9c44f74](https://gitlab.com/to-be-continuous/cypress/commit/9c44f74ba5ae87e9cb61cf8b9b49450b78365fae))

# [3.1.0](https://gitlab.com/to-be-continuous/cypress/compare/3.0.0...3.1.0) (2022-10-04)


### Features

* normalize reports ([f2e5c63](https://gitlab.com/to-be-continuous/cypress/commit/f2e5c63d75fa3bd788b3e4bb754c7fc3e29889cd))

# [3.0.0](https://gitlab.com/to-be-continuous/cypress/compare/2.1.0...3.0.0) (2022-08-05)


### Features

* adapative pipeline ([eedfa6b](https://gitlab.com/to-be-continuous/cypress/commit/eedfa6b7781d8337a99109341bcac0f0ad6f7c54))


### BREAKING CHANGES

* change default workflow from Branch pipeline to MR pipeline

# [2.1.0](https://gitlab.com/to-be-continuous/cypress/compare/2.0.1...2.1.0) (2022-05-01)


### Features

* configurable tracking image ([8e7987c](https://gitlab.com/to-be-continuous/cypress/commit/8e7987c2dd93e33f126804b64fc817ff2cfe7965))

## [2.0.1](https://gitlab.com/to-be-continuous/cypress/compare/2.0.0...2.0.1) (2021-10-07)


### Bug Fixes

* use master or main for production env ([89b94f0](https://gitlab.com/to-be-continuous/cypress/commit/89b94f076f7ceb905410a2b13c04de112a778944))

## [2.0.0](https://gitlab.com/to-be-continuous/cypress/compare/1.2.0...2.0.0) (2021-09-02)

### Features

* Change boolean variable behaviour ([b358e3b](https://gitlab.com/to-be-continuous/cypress/commit/b358e3b8d6e3f9ed23d782f1e78592693cbe34e6))

### BREAKING CHANGES

* boolean variable now triggered on explicit 'true' value

## [1.2.0](https://gitlab.com/to-be-continuous/cypress/compare/1.1.0...1.2.0) (2021-06-10)

### Features

* move group ([95571ff](https://gitlab.com/to-be-continuous/cypress/commit/95571ffcf1bf47676e8928b3efa6d41fd033379a))

## [1.1.0](https://gitlab.com/Orange-OpenSource/tbc/cypress/compare/1.0.0...1.1.0) (2021-05-18)

### Features

* add scoped variables support ([4c133d9](https://gitlab.com/Orange-OpenSource/tbc/cypress/commit/4c133d9c46576e3537695e1da624c6f91146b8a5))

## 1.0.0 (2021-05-06)

### Features

* initial release ([e946d37](https://gitlab.com/Orange-OpenSource/tbc/cypress/commit/e946d37a5b37c2fe4d4aa238039cbbfce7838aaf))
